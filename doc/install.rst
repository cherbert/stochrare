Installation
============

Dependencies
------------

- Python 3.6 or above
- numpy
- scipy
- matplotlib
- Optional: jupyter

stochrare only requires a running Python 3 install and standard scientific packages.
Tutorial notebooks require Jupyter.

Basic Install
-------------

The easiest way to install stochrare is from the Python Package Index, using the Python Package Installer::

  pip install stochrare

Development Mode Install
------------------------

If you intend to edit the code, you should install stochrare in development mode.

First clone the repository::

  git clone https://github.com/cbherbert/stochrare

Then go to the directory and install the package in development mode::

  cd stochrare
  pip install -e .
